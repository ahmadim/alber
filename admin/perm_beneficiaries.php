<?php

// include start file
require_once('../start.php');

// active page
$data['active'] = 'perm_beneficiaries';

// view single item
if(isset($_GET['action']) && $_GET['action'] == 'view'){
  $result = $db->query("
    SELECT perm_beneficiaries.*, user.*, charity.name as charity_name
    FROM `perm_beneficiaries`
    INNER JOIN `user` on  perm_beneficiaries.user_id = user.id
    left JOIN `charity` on user.charity_id = charity.id
    WHERE user.id = '".$_GET['id']."' AND user.level = 3 limit 1
  ");
  if ($result->num_rows == 0) {
    header('Location: /');
  }
  $data['item'] = $result->fetch_assoc();
  echo $twig->render('admin/admin-perm_beneficiaries-view.html', $data);
}


// get add new item
if(isset($_GET['action']) && $_GET['action'] == 'add'){
  $result = $db->query("SELECT id,name FROM charity");
  while($row = $result->fetch_assoc())
  {
    $data['charities'][] = $row;
  }
  echo $twig->render('admin/admin-perm_beneficiaries-add.html', $data);
}

// post add new item
if(isset($_POST['action']) && $_POST['action'] == 'add'){
  if($_POST['name'] != '' && $_POST['email'] != '' && $_POST['password'] != ''){
    $newUser = $db->query("INSERT INTO user (name, email, password, charity_id, level) VALUES ('".$_POST['name']."', '".$_POST['email']."', '".$_POST['password']."', '".$_POST['charity_id']."',  3   )") ;

    if ($db->query("INSERT INTO perm_beneficiaries (user_id, age) VALUES ('".$db->insert_id."', '".$_POST['age']."')")) {
      $data['msg'] = $_SESSION["msg"] = "تم إضافة البيانات بنجاح";
    } else {
      $data['msg'] = $_SESSION["msg"] = "حدث خطأ أثناء تحديث البيانات! الرجاء المحاولة مرة أخرى"; ;
      // die(mysqli_error($db));
    }
    header('Location: /admin/perm_beneficiaries.php');
  }else{
    $data['msg'] = $_SESSION["msg"] = "الرجاء إكمال تعبئة الحقول الضرورية";
    echo $twig->render('admin/admin-perm_beneficiaries-add.html', $data);
  }
}


// get edit item
if(isset($_GET['action']) && $_GET['action'] == 'edit'){
  // $result = $db->query("SELECT * FROM user WHERE id = '".$_GET['id']."' limit 1");
  $result = $db->query("
    SELECT perm_beneficiaries.*, user.*
    FROM `perm_beneficiaries`
    INNER JOIN `user` on  perm_beneficiaries.user_id = user.id
    WHERE user.id = '".$_GET['id']."' limit 1
  ");
  if ($result->num_rows == 0) {
    header('Location: /');
  }
  $data['item'] = $result->fetch_assoc();

  // get charity list
  $result = $db->query("SELECT id,name FROM charity");
  while($row = $result->fetch_assoc())
  {
    $data['charities'][] = $row;
  }
  echo $twig->render('admin/admin-perm_beneficiaries-edit.html', $data);
}


// post edit item
if(isset($_POST['action']) && $_POST['action'] == 'update'){
  if ($db->query("
      UPDATE user a
      JOIN perm_beneficiaries b ON a.id = b.user_id
      SET a.name = '".$_POST['name']."', email = '".$_POST['email']."' , password = '".$_POST['password']."' , charity_id = '".$_POST['charity_id']."' ,
      b.age = '".$_POST['age']."'
      WHERE b.user_id = '".$_POST['id']."'
    ")) {
    $data['msg'] = $_SESSION["msg"] = "تم تحديث البيانات بنجاح";
  } else {
    $data['msg'] = $_SESSION["msg"] = "حدث خطأ أثناء تحديث البيانات! الرجاء المحاولة مرة أخرى"; ;
  }
  header('Location: /admin/perm_beneficiaries.php');
}


// delete item
if(isset($_GET['action']) && $_GET['action'] == 'delete'){
  // $result = $db->query("
  //   DELETE user, perm_beneficiaries
  //   FROM user
  //   INNER JOIN perm_beneficiaries ON user.id = perm_beneficiaries.user_id
  //   WHERE perm_beneficiaries.user_id = '".$_GET['id']."'
  // ");
  // if(!$result){
  //   die(mysqli_error($db));
  // }
  $result = $db->query("DELETE FROM user WHERE id = '".$_GET['id']."' limit 1");
  header('Location: /admin/perm_beneficiaries.php');
}


// list all items
// if there is no action at all
if(!isset($_POST['action']) && !isset($_GET['action'])) {
  $result = $db->query("
    SELECT perm_beneficiaries.*, user.*, charity.name as charity_name
    FROM `perm_beneficiaries`
    INNER JOIN `user` on  perm_beneficiaries.user_id = user.id
    left JOIN `charity` on user.charity_id = charity.id
    order by perm_beneficiaries.id desc
  ");

  while($row = $result->fetch_assoc())
  {
    $data['results'][] = $row; // set each row in the results array
  }
  echo $twig->render('admin/admin-perm_beneficiaries-list.html', $data);
}
