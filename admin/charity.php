<?php

// include start file
require_once('../start.php');

// active page
$data['active'] = 'charity';

// view single item
if(isset($_GET['action']) && $_GET['action'] == 'view'){
  $result = $db->query("SELECT * FROM charity WHERE id = '".$_GET['id']."' AND admin_id = '".$user['id']."' limit 1");
  if ($result->num_rows == 0) {
    header('Location: /');
  }
  $data['item'] = $result->fetch_assoc();
  echo $twig->render('admin/admin-charity-view.html', $data);
}


// get add new item
if(isset($_GET['action']) && $_GET['action'] == 'add'){
  echo $twig->render('admin/admin-charity-add.html', $data);
}

// post add new item
if(isset($_POST['action']) && $_POST['action'] == 'add'){
  if($_POST['name'] != '' && $_POST['email'] != ''){
    if ($db->query("INSERT INTO charity (name, phone, email) VALUES ('".$_POST['name']."', '".($_POST['phone']?$_POST['phone']:0)."', '".$_POST['email']."')")) {
      $data['msg'] = $_SESSION["msg"] = "تم تحديث البيانات بنجاح";
    } else {
      $data['msg'] = $_SESSION["msg"] = "حدث خطأ أثناء تحديث البيانات! الرجاء المحاولة مرة أخرى"; ;
    }
    header('Location: /admin/charity.php');
  }else{
    $data['msg'] = $_SESSION["msg"] = "الرجاء إكمال تعبئة الحقول الضرورية";
    echo $twig->render('admin/admin-charity-add.html', $data);
  }
}


// get edit item
if(isset($_GET['action']) && $_GET['action'] == 'edit'){
  $result = $db->query("SELECT * FROM charity WHERE id = '".$_GET['id']."' AND admin_id = '".$user['id']."' limit 1");
  if ($result->num_rows == 0) {
    header('Location: /');
  }
  $data['item'] = $result->fetch_assoc();
  echo $twig->render('admin/admin-charity-edit.html', $data);
}


// post edit item
if(isset($_POST['action']) && $_POST['action'] == 'update'){
  if ($db->query("UPDATE charity SET name = '".$_POST['name']."' , email = '".$_POST['email']."' , phone = '".$_POST['phone']."'  WHERE id = '".$_POST['id']."' " )) {
    $data['msg'] = $_SESSION["msg"] = "تم تحديث البيانات بنجاح";
  } else {
    $data['msg'] = $_SESSION["msg"] = "حدث خطأ أثناء تحديث البيانات! الرجاء المحاولة مرة أخرى"; ;
  }
  header('Location: /admin/charity.php');
  // echo $twig->render('admin/admin-charity-edit.html', $data);
}


// delete item
if(isset($_GET['action']) && $_GET['action'] == 'delete'){
  $result = $db->query("DELETE FROM charity WHERE id = '".$_GET['id']."' AND admin_id = '".$user['id']."' limit 1");
  header('Location: /admin/charity.php');
}


// list all items
// if there is no action at all
if(!isset($_POST['action']) && !isset($_GET['action'])) {
  $result = $db->query("SELECT * FROM charity WHERE admin_id = '".$user['id']."' order by id desc ");
  while($row = $result->fetch_assoc())
  {
    $data['results'][] = $row; // set each row in the results array
  }
  echo $twig->render('admin/admin-charity-list.html', $data);
}
