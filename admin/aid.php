<?php

// include start file
require_once('../start.php');

// active page
$data['active'] = 'aid';

// view single item
if(isset($_GET['action']) && $_GET['action'] == 'view'){
  $result = $db->query("SELECT * FROM aid WHERE id = '".$_GET['id']."' limit 1");
  if ($result->num_rows == 0) {
    header('Location: /');
  }
  $data['item'] = $result->fetch_assoc();
  echo $twig->render('admin/admin-aid-view.html', $data);
}


// get add new item
if(isset($_GET['action']) && $_GET['action'] == 'add'){
  echo $twig->render('admin/admin-aid-add.html', $data);
}

// post add new item
if(isset($_POST['action']) && $_POST['action'] == 'add'){
  if($_POST['name'] != ''){
    if ($db->query("INSERT INTO aid (name, details) VALUES ('".$_POST['name']."', '".$_POST['details']."')")) {
      $data['msg'] = $_SESSION["msg"] = "تمت الإضافة بنجاح";
    } else {
      $data['msg'] = $_SESSION["msg"] = "حدث خطأ أثناء تحديث البيانات! الرجاء المحاولة مرة أخرى"; ;
    }
    header('Location: /admin/aid.php');
  }else{
    $data['msg'] = $_SESSION["msg"] = "الرجاء إكمال تعبئة الحقول الضرورية";
    echo $twig->render('admin/admin-aid-add.html', $data);
  }
}


// get edit item
if(isset($_GET['action']) && $_GET['action'] == 'edit'){
  $result = $db->query("SELECT * FROM aid WHERE id = '".$_GET['id']."'  limit 1");
  if ($result->num_rows == 0) {
    header('Location: /');
  }
  $data['item'] = $result->fetch_assoc();
  echo $twig->render('admin/admin-aid-edit.html', $data);
}


// post edit item
if(isset($_POST['action']) && $_POST['action'] == 'update'){
  if ($db->query("UPDATE aid SET name = '".$_POST['name']."' , details = '".$_POST['details']."'  WHERE id = '".$_POST['id']."' " )) {
    $data['msg'] = $_SESSION["msg"] = "تم تحديث البيانات بنجاح";
  } else {
    $data['msg'] = $_SESSION["msg"] = "حدث خطأ أثناء تحديث البيانات! الرجاء المحاولة مرة أخرى"; ;
  }
  header('Location: /admin/aid.php');
  // echo $twig->render('admin/admin-aid-edit.html', $data);
}


// delete item
if(isset($_GET['action']) && $_GET['action'] == 'delete'){
  $result = $db->query("DELETE FROM aid WHERE id = '".$_GET['id']."'   limit 1");
  header('Location: /admin/aid.php');
}


// list all items
// if there is no action at all
if(!isset($_POST['action']) && !isset($_GET['action'])) {
  $result = $db->query("SELECT * FROM aid order by id desc");
  while($row = $result->fetch_assoc())
  {
    $data['results'][] = $row; // set each row in the results array
  }
  echo $twig->render('admin/admin-aid-list.html', $data);
}
