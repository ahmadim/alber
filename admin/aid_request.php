<?php

// include start file
require_once('../start.php');

// active page
$data['active'] = 'aid_request';

// view single item
if(isset($_GET['action']) && $_GET['action'] == 'view'){
  $result = $db->query("
    SELECT aid_request.*, user.name as user_name
    FROM `aid_request`
    INNER JOIN `user` on aid_request.user_id = user.id
    WHERE aid_request.id = '".$_GET['id']."' limit 1
  ");
  if ($result->num_rows == 0) {
    header('Location: /');
  }
  $data['item'] = $result->fetch_assoc();
  echo $twig->render('admin/admin-aid_request-view.html', $data);
}


// get add new item
if(isset($_GET['action']) && $_GET['action'] == 'add'){
  $result = $db->query("SELECT id,name FROM charity");
  while($row = $result->fetch_assoc())
  {
    $data['charities'][] = $row;
  }
  echo $twig->render('admin/admin-aid_request-add.html', $data);
}

// post add new item
if(isset($_POST['action']) && $_POST['action'] == 'add'){
  if($_POST['name'] != '' && $_POST['details'] != ''){
    if ($db->query("INSERT INTO aid_request (name, details, charity_id, user_id,date) VALUES ('".$_POST['name']."', '".$_POST['details']."', '".$_POST['charity_id']."' , '".$user['id']."' , '".date('Y-m-d H:i:s')."'  )")) {
      $data['msg'] = $_SESSION["msg"] = "تمت الإضافة بنجاح";
    } else {
      $data['msg'] = $_SESSION["msg"] = "حدث خطأ أثناء تحديث البيانات! الرجاء المحاولة مرة أخرى"; ;
      // die(mysqli_error($db));
    }
    header('Location: /admin/aid_request.php');
  }else{
    $data['msg'] = $_SESSION["msg"] = "الرجاء إكمال تعبئة الحقول الضرورية";
    echo $twig->render('admin/admin-aid_request-add.html', $data);
  }
}


// get edit item
if(isset($_GET['action']) && $_GET['action'] == 'edit'){
  $result = $db->query("SELECT * FROM aid_request WHERE id = '".$_GET['id']."'  limit 1");
  if ($result->num_rows == 0) {
    header('Location: /');
  }
  $data['item'] = $result->fetch_assoc();

  $result = $db->query("SELECT id,name FROM charity");
  while($row = $result->fetch_assoc())
  {
    $data['charities'][] = $row;
  }
  echo $twig->render('admin/admin-aid_request-edit.html', $data);
}


// post edit item
if(isset($_POST['action']) && $_POST['action'] == 'update'){
  if ($db->query("UPDATE aid_request SET name = '".$_POST['name']."' , charity_id = '".$_POST['charity_id']."',  details = '".$_POST['details']."'  WHERE id = '".$_POST['id']."' " )) {
    $data['msg'] = $_SESSION["msg"] = "تم تحديث البيانات بنجاح";
  } else {
    $data['msg'] = $_SESSION["msg"] = "حدث خطأ أثناء تحديث البيانات! الرجاء المحاولة مرة أخرى"; ;
  }
  header('Location: /admin/aid_request.php');
  // echo $twig->render('admin/admin-aid_request-edit.html', $data);
}


// delete item
if(isset($_GET['action']) && $_GET['action'] == 'delete'){
  $result = $db->query("DELETE FROM aid_request WHERE id = '".$_GET['id']."' limit 1");
  header('Location: /admin/aid_request.php');
}


// list all items
// if there is no action at all
if(!isset($_POST['action']) && !isset($_GET['action'])) {
  $result = $db->query("
  SELECT aid_request.*, aid.name as aid_name, aid.id as aid_id,
  user.id as user_id, user.name as user_name,
  charity.name as charity_name
  FROM `aid_request`
  left JOIN `aid` on aid_request.aid_id = aid.id
  left JOIN `user` on aid_request.user_id = user.id
  left JOIN `charity` on aid_request.charity_id = charity.id
  order by id desc
  ");
  while($row = $result->fetch_assoc())
  {
    $data['results'][] = $row; // set each row in the results array
  }
  echo $twig->render('admin/admin-aid_request-list.html', $data);
}
