<?php

// include start file
require_once('../start.php');

// active page
$data['active'] = 'researcher';

// view single item
if(isset($_GET['action']) && $_GET['action'] == 'view'){
  $result = $db->query("
    SELECT user.*, charity.name as charity_name
    FROM `user`
    INNER JOIN `charity` on user.charity_id = charity.id
    WHERE user.id = '".$_GET['id']."' AND user.level = 2 limit 1
     ;
  ");
  if ($result->num_rows == 0) {
    header('Location: /');
  }
  $data['item'] = $result->fetch_assoc();
  echo $twig->render('admin/admin-researcher-view.html', $data);
}


// get add new item
if(isset($_GET['action']) && $_GET['action'] == 'add'){
  $result = $db->query("SELECT id,name FROM charity");
  while($row = $result->fetch_assoc())
  {
    $data['charities'][] = $row;
  }
  echo $twig->render('admin/admin-researcher-add.html', $data);
}

// post add new item
if(isset($_POST['action']) && $_POST['action'] == 'add'){
  if($_POST['name'] != '' && $_POST['email'] != '' && $_POST['password'] != ''){
    if ($db->query("INSERT INTO user (name, email, password, charity_id, level) VALUES ('".$_POST['name']."', '".$_POST['email']."', '".$_POST['password']."', '".$_POST['charity_id']."', 2    )")) {
      $data['msg'] = $_SESSION["msg"] = "تم إضافة البيانات بنجاح";
    } else {
      $data['msg'] = $_SESSION["msg"] = "حدث خطأ أثناء تحديث البيانات! الرجاء المحاولة مرة أخرى"; ;
    }
    header('Location: /admin/researcher.php');
  }else{
    $data['msg'] = $_SESSION["msg"] = "الرجاء إكمال تعبئة الحقول الضرورية";
    echo $twig->render('admin/admin-researcher-add.html', $data);
  }
}


// get edit item
if(isset($_GET['action']) && $_GET['action'] == 'edit'){
  $result = $db->query("SELECT * FROM user WHERE id = '".$_GET['id']."' limit 1");
  if ($result->num_rows == 0) {
    header('Location: /');
  }
  $data['item'] = $result->fetch_assoc();

  // get charity list
  $result = $db->query("SELECT id,name FROM charity");
  while($row = $result->fetch_assoc())
  {
    $data['charities'][] = $row;
  }
  echo $twig->render('admin/admin-researcher-edit.html', $data);
}


// post edit item
if(isset($_POST['action']) && $_POST['action'] == 'update'){


  if ($db->query("UPDATE user SET name = '".$_POST['name']."' , email = '".$_POST['email']."' , password = '".$_POST['password']."' , charity_id = '".$_POST['charity_id']."'  WHERE id = '".$_POST['id']."' " )) {
    $data['msg'] = $_SESSION["msg"] = "تم تحديث البيانات بنجاح";
  } else {
    $data['msg'] = $_SESSION["msg"] = "حدث خطأ أثناء تحديث البيانات! الرجاء المحاولة مرة أخرى"; ;
  }
  header('Location: /admin/researcher.php');
  // echo $twig->render('admin/admin-researcher-edit.html', $data);
}


// delete item
if(isset($_GET['action']) && $_GET['action'] == 'delete'){
  $result = $db->query("DELETE FROM user WHERE id = '".$_GET['id']."' limit 1");
  header('Location: /admin/researcher.php');
}


// list all items
// if there is no action at all
if(!isset($_POST['action']) && !isset($_GET['action'])) {
  $result = $db->query("
    SELECT user.*, charity.name as charity_name
    FROM `user`
    INNER JOIN `charity` on user.charity_id = charity.id
    WHERE level = 2
    order by id desc
  ");
  while($row = $result->fetch_assoc())
  {
    $data['results'][] = $row; // set each row in the results array
  }
  echo $twig->render('admin/admin-researcher-list.html', $data);
}
