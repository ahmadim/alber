<?php

// include start file
require_once('../start.php');

// active page
$data['active'] = 'beneficiary_categories';

// view single item
if(isset($_GET['action']) && $_GET['action'] == 'view'){
  $result = $db->query("SELECT * FROM beneficiary_categories WHERE id = '".$_GET['id']."' limit 1");
  if ($result->num_rows == 0) {
    header('Location: /');
  }
  $data['item'] = $result->fetch_assoc();
  echo $twig->render('admin/admin-beneficiary_categories-view.html', $data);
}


// get add new item
if(isset($_GET['action']) && $_GET['action'] == 'add'){
  echo $twig->render('admin/admin-beneficiary_categories-add.html', $data);
}

// post add new item
if(isset($_POST['action']) && $_POST['action'] == 'add'){
  if($_POST['name'] != ''){
    if ($db->query("INSERT INTO beneficiary_categories (name, amount1, amount2) VALUES ('".$_POST['name']."', '".$_POST['amount1']."' , '".$_POST['amount2']."')")) {
      $data['msg'] = $_SESSION["msg"] = "تمت الإضافة بنجاح";
    } else {
      $data['msg'] = $_SESSION["msg"] = "حدث خطأ أثناء تحديث البيانات! الرجاء المحاولة مرة أخرى"; ;
    }
    header('Location: /admin/beneficiary_categories.php');
  }else{
    $data['msg'] = $_SESSION["msg"] = "الرجاء إكمال تعبئة الحقول الضرورية";
    echo $twig->render('admin/admin-beneficiary_categories-add.html', $data);
  }
}


// get edit item
if(isset($_GET['action']) && $_GET['action'] == 'edit'){
  $result = $db->query("SELECT * FROM beneficiary_categories WHERE id = '".$_GET['id']."'  limit 1");
  if ($result->num_rows == 0) {
    header('Location: /');
  }
  $data['item'] = $result->fetch_assoc();
  echo $twig->render('admin/admin-beneficiary_categories-edit.html', $data);
}


// post edit item
if(isset($_POST['action']) && $_POST['action'] == 'update'){
  if ($db->query("UPDATE beneficiary_categories SET name = '".$_POST['name']."' , amount1 = '".$_POST['amount1']."'  , amount2 = '".$_POST['amount2']."'  WHERE id = '".$_POST['id']."' " )) {
    $data['msg'] = $_SESSION["msg"] = "تم تحديث البيانات بنجاح";
  } else {
    $data['msg'] = $_SESSION["msg"] = "حدث خطأ أثناء تحديث البيانات! الرجاء المحاولة مرة أخرى"; ;
  }
  header('Location: /admin/beneficiary_categories.php');
  // echo $twig->render('admin/admin-beneficiary_categories-edit.html', $data);
}


// delete item
if(isset($_GET['action']) && $_GET['action'] == 'delete'){
  $result = $db->query("DELETE FROM beneficiary_categories WHERE id = '".$_GET['id']."'   limit 1");
  header('Location: /admin/beneficiary_categories.php');
}


// list all items
// if there is no action at all
if(!isset($_POST['action']) && !isset($_GET['action'])) {
  $result = $db->query("SELECT * FROM beneficiary_categories order by id desc");
  while($row = $result->fetch_assoc())
  {
    $data['results'][] = $row; // set each row in the results array
  }
  echo $twig->render('admin/admin-beneficiary_categories-list.html', $data);
}
