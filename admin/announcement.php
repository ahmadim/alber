<?php

// include start file
require_once('../start.php');

// active page
$data['active'] = 'announcement';

// view single item
if(isset($_GET['action']) && $_GET['action'] == 'view'){
  $result = $db->query("
    SELECT announcement.*, user.name as user_name
    FROM `announcement`
    INNER JOIN `user` on announcement.user_id = user.id
    WHERE announcement.id = '".$_GET['id']."' limit 1
  ");
  if ($result->num_rows == 0) {
    header('Location: /');
  }
  $data['item'] = $result->fetch_assoc();
  echo $twig->render('admin/admin-announcement-view.html', $data);
}


// get add new item
if(isset($_GET['action']) && $_GET['action'] == 'add'){
  $result = $db->query("SELECT id,name FROM charity");
  while($row = $result->fetch_assoc())
  {
    $data['charities'][] = $row;
  }
  echo $twig->render('admin/admin-announcement-add.html', $data);
}

// post add new item
if(isset($_POST['action']) && $_POST['action'] == 'add'){
  if($_POST['name'] != '' && $_POST['details'] != ''){
    if ($db->query("INSERT INTO announcement (name, details, charity_id, user_id,date) VALUES ('".$_POST['name']."', '".$_POST['details']."', '".$_POST['charity_id']."' , '".$user['id']."' , '".date('Y-m-d H:i:s')."'  )")) {
      $data['msg'] = $_SESSION["msg"] = "تمت الإضافة بنجاح";
    } else {
      $data['msg'] = $_SESSION["msg"] = "حدث خطأ أثناء تحديث البيانات! الرجاء المحاولة مرة أخرى"; ;
      // die(mysqli_error($db));
    }
    header('Location: /admin/announcement.php');
  }else{
    $data['msg'] = $_SESSION["msg"] = "الرجاء إكمال تعبئة الحقول الضرورية";
    echo $twig->render('admin/admin-announcement-add.html', $data);
  }
}


// get edit item
if(isset($_GET['action']) && $_GET['action'] == 'edit'){
  $result = $db->query("SELECT * FROM announcement WHERE id = '".$_GET['id']."'  limit 1");
  if ($result->num_rows == 0) {
    header('Location: /');
  }
  $data['item'] = $result->fetch_assoc();

  $result = $db->query("SELECT id,name FROM charity");
  while($row = $result->fetch_assoc())
  {
    $data['charities'][] = $row;
  }
  echo $twig->render('admin/admin-announcement-edit.html', $data);
}


// post edit item
if(isset($_POST['action']) && $_POST['action'] == 'update'){
  if ($db->query("UPDATE announcement SET name = '".$_POST['name']."' , charity_id = '".$_POST['charity_id']."',  details = '".$_POST['details']."'  WHERE id = '".$_POST['id']."' " )) {
    $data['msg'] = $_SESSION["msg"] = "تم تحديث البيانات بنجاح";
  } else {
    $data['msg'] = $_SESSION["msg"] = "حدث خطأ أثناء تحديث البيانات! الرجاء المحاولة مرة أخرى"; ;
  }
  header('Location: /admin/announcement.php');
  // echo $twig->render('admin/admin-announcement-edit.html', $data);
}


// delete item
if(isset($_GET['action']) && $_GET['action'] == 'delete'){
  $result = $db->query("DELETE FROM announcement WHERE id = '".$_GET['id']."' limit 1");
  header('Location: /admin/announcement.php');
}


// list all items
// if there is no action at all
if(!isset($_POST['action']) && !isset($_GET['action'])) {
  $result = $db->query("
  SELECT announcement.*, user.name as user_name , charity.name as charity_name
  FROM `announcement`
  INNER JOIN `user` on announcement.user_id = user.id
  INNER JOIN `charity` on announcement.charity_id = charity.id
  order by id desc
  ");
  while($row = $result->fetch_assoc())
  {
    $data['results'][] = $row; // set each row in the results array
  }
  echo $twig->render('admin/admin-announcement-list.html', $data);
}
