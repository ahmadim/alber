<?php

// start session
session_start();

// Load Twig (Template System) :: (check http://twig.sensiolabs.org)
// -----
require_once('Libraries/Twig/Autoloader.php');
Twig_Autoloader::register();
$loader = new Twig_Loader_Filesystem(__DIR__.'/views');
$twig   = new Twig_Environment($loader);


// Connect to database
// -----
$db = @mysqli_connect("localhost","root","","alber");
@mysqli_set_charset($db,"utf8");
if (mysqli_connect_errno())
{
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
}



// Get user info and set it in $data array to use it in views(html files) if he is logged in
// -----
if(isset($_COOKIE['login']))
{
  // get my info
  $result = $db->query("SELECT * FROM user WHERE id = '".$_COOKIE['id']."' limit 1");
  $data['user'] = $user = $result->fetch_assoc();

  // if per beneficiary
  if($user['level'] ==3){
    $result = $db->query("
      SELECT perm_beneficiaries.*, user.*, charity.name as charity_name
      FROM `perm_beneficiaries`
      INNER JOIN `user` on  perm_beneficiaries.user_id = user.id
      left JOIN `charity` on user.charity_id = charity.id
      WHERE user.id = '".$_COOKIE['id']."' AND user.level = 3 limit 1
    ");
    $data['user'] = $user = $result->fetch_assoc();
  }


  $data['logged_in']  = $_COOKIE['login'];
  $data['name']       = $user['name'];
  $data['level']      = $user['level'];

  //level name
  switch ($user['level']) {
      case 1:
          $data['level_name']= "مدير";
          break;
      case 2:
          $data['level_name']= "باحث";
          break;
      case 3:
          $data['level_name']= "مستفيد دائم";
          break;
      case 4:
          $data['level_name']= "مستفيد مؤقت";
          break;
      default:
          $data['level_name']= "زائر";
  }
}
