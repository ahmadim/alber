<?php

// # This file aims to open the desired html static template page instead of creating a lot of php files like (procurement-terms.php and procurement-advantages .. etc)
// so we can open any html file just by open static.php?page=MY_STATIC_FILE, which it should be located in (views/front/MY_STATIC_FILE.html)
// ---------


// include start file
require_once('start.php');

// Views Data
$data['active'] = $_GET['page'];

// Render 'Page' Template if exists
$page_file = 'views/front/'.$_GET['page'].'.html';
if (file_exists($page_file))
{
  echo $twig->render('front/'.$_GET['page'].'.html', $data);
}
else
{
  // if html template not found, open 404.html "not found" page instead
  echo $twig->render('front/404.html', $data);
}
