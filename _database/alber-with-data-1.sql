-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `advanteges`;
CREATE TABLE `advanteges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `advanteges` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `aid`;
CREATE TABLE `aid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `details` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `aid` (`id`, `name`, `details`) VALUES
(1,	'مساعدة أولى',	'تفاصيل المساعدة'),
(2,	'مساعدة ثانية',	'تفاصيل المساعدة هنا '),
(3,	'مساعدة ثالثة',	'تفاصيل المساعدة هنا \r\nهنا\r\nوهنا');

DROP TABLE IF EXISTS `aid_request`;
CREATE TABLE `aid_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `note` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `aid_id` int(11) NOT NULL,
  `researcher_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `charity_id` int(11) NOT NULL,
  `date` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `aid_id` (`aid_id`),
  KEY `user_id` (`user_id`),
  KEY `charity_id` (`charity_id`),
  CONSTRAINT `aid_request_ibfk_1` FOREIGN KEY (`aid_id`) REFERENCES `aid` (`id`),
  CONSTRAINT `aid_request_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `aid_request_ibfk_3` FOREIGN KEY (`charity_id`) REFERENCES `charity` (`id`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `aid_request` (`id`, `note`, `state`, `reason`, `aid_id`, `researcher_id`, `user_id`, `charity_id`, `date`) VALUES
(1,	'ملاحظة',	'إنتظار الموافقة',	NULL,	1,	2,	3,	2,	'2017-03-14 02:26:04');

DROP TABLE IF EXISTS `announcement`;
CREATE TABLE `announcement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(255) DEFAULT NULL,
  `sub` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `details` text NOT NULL,
  `charity_id` int(6) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `announcement_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `announcement` (`id`, `date`, `name`, `sub`, `user_id`, `details`, `charity_id`) VALUES
(1,	'2017-03-13 18:18:28',	'إعلان للجمعية الأولى',	NULL,	1,	'تفاصيل الإعلان',	1),
(2,	'2017-03-13 18:18:44',	'إعلان للجميعة الثانية',	NULL,	1,	'تفاصيل الإعلان \r\nهنا',	2),
(3,	'2017-03-13 18:19:02',	'إعلان للجميعة الثالثة',	NULL,	1,	'تفاصيل إعلان الجمعية الثالثة هنا ..',	3);

DROP TABLE IF EXISTS `beneficiary_categories`;
CREATE TABLE `beneficiary_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `amount1` int(11) DEFAULT NULL,
  `amount2` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `beneficiary_categories` (`id`, `name`, `amount1`, `amount2`) VALUES
(1,	'فئة أولى',	2000,	3500),
(2,	'فئة ثانية',	3500,	5000),
(3,	'فئة ثالثة',	5000,	8000);

DROP TABLE IF EXISTS `charity`;
CREATE TABLE `charity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `p_o_box` varchar(12) DEFAULT NULL,
  `post_code` varchar(12) DEFAULT NULL,
  `fax` varchar(12) DEFAULT NULL,
  `phone` varchar(12) NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `admin_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `charity` (`id`, `name`, `p_o_box`, `post_code`, `fax`, `phone`, `email`, `admin_id`) VALUES
(1,	'جمعية أولى',	NULL,	NULL,	NULL,	'28000011',	'first@charity.com',	1),
(2,	'الجمعية الثانية',	NULL,	NULL,	NULL,	'29000000',	'second@charity.com',	1),
(3,	'الجمعية الثالثة',	NULL,	NULL,	NULL,	'240001123',	'third@charity.com',	1);

DROP TABLE IF EXISTS `evalution`;
CREATE TABLE `evalution` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_char` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `evalution` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `perm_beneficiaries`;
CREATE TABLE `perm_beneficiaries` (
  `age` int(11) DEFAULT NULL,
  `loc` varchar(255) DEFAULT NULL,
  `telephone` int(11) DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `phone2` int(11) DEFAULT NULL,
  `work` varchar(255) DEFAULT NULL,
  `job` varchar(255) DEFAULT NULL,
  `m_income` int(11) DEFAULT NULL,
  `y_income` int(11) DEFAULT NULL,
  `reward` varchar(11) CHARACTER SET utf32 DEFAULT NULL,
  `trade` varchar(11) CHARACTER SET utf32 DEFAULT NULL,
  `guarantee` varchar(11) CHARACTER SET utf32 DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf32 DEFAULT NULL,
  `chile_num` int(11) DEFAULT NULL,
  `boy` int(11) DEFAULT NULL,
  `girl` int(11) DEFAULT NULL,
  `servant` varchar(255) DEFAULT NULL,
  `clothes` varchar(255) DEFAULT NULL,
  `home` varchar(255) DEFAULT NULL,
  `amount_rent` int(11) DEFAULT NULL,
  `type_home` varchar(255) DEFAULT NULL,
  `s_home` varchar(255) DEFAULT NULL,
  `s_furniture` varchar(255) DEFAULT NULL,
  `note_home` longtext,
  `health` varchar(255) DEFAULT NULL,
  `s_health` longtext,
  `name_per` varchar(255) DEFAULT NULL,
  `job_per` varchar(255) DEFAULT NULL,
  `id_num_per` int(11) DEFAULT NULL,
  `relative_per` varchar(255) DEFAULT NULL,
  `no_agency` int(11) DEFAULT NULL,
  `date_agency` date DEFAULT NULL,
  `type_agency` varchar(255) DEFAULT NULL,
  `duration_agency` varchar(255) DEFAULT NULL,
  `source_agency` varchar(255) DEFAULT NULL,
  `teleph_per` int(11) DEFAULT NULL,
  `phone_per` int(11) DEFAULT NULL,
  `note_resarch` longtext,
  `note_social` longtext,
  `care_num` int(11) DEFAULT NULL,
  `type_care1` varchar(255) DEFAULT NULL,
  `model_care1` varchar(255) DEFAULT NULL,
  `farms` varchar(255) DEFAULT NULL,
  `animals` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `resarcher_id` int(11) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `employee` varchar(255) DEFAULT NULL,
  `wife` varchar(255) DEFAULT NULL,
  `type` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `perm_beneficiaries_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `perm_beneficiaries` (`age`, `loc`, `telephone`, `phone`, `phone2`, `work`, `job`, `m_income`, `y_income`, `reward`, `trade`, `guarantee`, `status`, `chile_num`, `boy`, `girl`, `servant`, `clothes`, `home`, `amount_rent`, `type_home`, `s_home`, `s_furniture`, `note_home`, `health`, `s_health`, `name_per`, `job_per`, `id_num_per`, `relative_per`, `no_agency`, `date_agency`, `type_agency`, `duration_agency`, `source_agency`, `teleph_per`, `phone_per`, `note_resarch`, `note_social`, `care_num`, `type_care1`, `model_care1`, `farms`, `animals`, `id`, `user_id`, `resarcher_id`, `state`, `employee`, `wife`, `type`) VALUES
(22,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	3,	NULL,	NULL,	NULL,	NULL,	1),
(40,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	2,	4,	NULL,	NULL,	NULL,	NULL,	1);

DROP TABLE IF EXISTS `social`;
CREATE TABLE `social` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Conditions` varchar(255) NOT NULL,
  `verification_elem` varchar(255) NOT NULL,
  `section` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `temp_beneficiaries_cat`;
CREATE TABLE `temp_beneficiaries_cat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `loc` varchar(255) NOT NULL,
  `telephone` int(11) NOT NULL,
  `phone` int(11) NOT NULL,
  `work` varchar(255) NOT NULL,
  `teleph_work` int(11) NOT NULL,
  `name_per` varchar(255) NOT NULL,
  `work_per` varchar(255) NOT NULL,
  `teleph_per` int(11) NOT NULL,
  `phone_per` int(11) NOT NULL,
  `telepho_per_work` int(11) NOT NULL,
  `m_income` int(11) NOT NULL,
  `y_income` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_resarch` int(11) DEFAULT NULL,
  `id_num_per` int(11) NOT NULL,
  `home` varchar(255) NOT NULL,
  `typehome` varchar(255) NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `temp_familly`;
CREATE TABLE `temp_familly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_b` int(11) NOT NULL,
  `child` varchar(255) CHARACTER SET utf8 NOT NULL,
  `child_num` int(11) NOT NULL,
  `reason` varchar(255) CHARACTER SET utf8 NOT NULL,
  `wife` varchar(255) CHARACTER SET utf8 NOT NULL,
  `car` int(11) NOT NULL,
  `farmer` varchar(22) CHARACTER SET utf8 NOT NULL,
  `animals` varchar(255) CHARACTER SET utf8 NOT NULL,
  `mosque` varchar(255) CHARACTER SET utf8 NOT NULL,
  `aimam` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `temp_individule`;
CREATE TABLE `temp_individule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_b` int(11) NOT NULL,
  `status` varchar(255) CHARACTER SET utf8 NOT NULL,
  `date_st` varchar(255) CHARACTER SET utf8 NOT NULL,
  `name_lessor` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `amount_rent` int(11) DEFAULT NULL,
  `type_mony` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `level` enum('1','2','3','4','5') CHARACTER SET latin1 DEFAULT '5',
  `password` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `charity_id` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `user` (`id`, `name`, `email`, `level`, `password`, `charity_id`) VALUES
(1,	'Admin',	'a@a.com',	'1',	'123',	0),
(2,	'باحث أول',	'b@b.com',	'2',	'123',	1),
(3,	'عبدالله',	'c@c.com',	'3',	'123',	2),
(4,	'أسامة',	'd@d.com',	'3',	'123',	3);

-- 2017-03-13 18:28:46
