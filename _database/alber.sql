-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `advanteges`;
CREATE TABLE `advanteges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `advanteges` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `aid`;
CREATE TABLE `aid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `details` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `aid_request`;
CREATE TABLE `aid_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `note` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `aid_id` int(11) NOT NULL,
  `researcher_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `charity_id` int(11) NOT NULL,
  `date` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `aid_id` (`aid_id`),
  KEY `user_id` (`user_id`),
  KEY `charity_id` (`charity_id`),
  CONSTRAINT `aid_request_ibfk_1` FOREIGN KEY (`aid_id`) REFERENCES `aid` (`id`),
  CONSTRAINT `aid_request_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `aid_request_ibfk_3` FOREIGN KEY (`charity_id`) REFERENCES `charity` (`id`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `announcement`;
CREATE TABLE `announcement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(255) DEFAULT NULL,
  `sub` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `details` text NOT NULL,
  `charity_id` int(6) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `announcement_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `beneficiary_categories`;
CREATE TABLE `beneficiary_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `amount1` int(11) DEFAULT NULL,
  `amount2` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `charity`;
CREATE TABLE `charity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `p_o_box` varchar(12) DEFAULT NULL,
  `post_code` varchar(12) DEFAULT NULL,
  `fax` varchar(12) DEFAULT NULL,
  `phone` varchar(12) NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `admin_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `evalution`;
CREATE TABLE `evalution` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_char` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `evalution` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `perm_beneficiaries`;
CREATE TABLE `perm_beneficiaries` (
  `age` int(11) DEFAULT NULL,
  `loc` varchar(255) DEFAULT NULL,
  `telephone` int(11) DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `phone2` int(11) DEFAULT NULL,
  `work` varchar(255) DEFAULT NULL,
  `job` varchar(255) DEFAULT NULL,
  `m_income` int(11) DEFAULT NULL,
  `y_income` int(11) DEFAULT NULL,
  `reward` varchar(11) CHARACTER SET utf32 DEFAULT NULL,
  `trade` varchar(11) CHARACTER SET utf32 DEFAULT NULL,
  `guarantee` varchar(11) CHARACTER SET utf32 DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf32 DEFAULT NULL,
  `chile_num` int(11) DEFAULT NULL,
  `boy` int(11) DEFAULT NULL,
  `girl` int(11) DEFAULT NULL,
  `servant` varchar(255) DEFAULT NULL,
  `clothes` varchar(255) DEFAULT NULL,
  `home` varchar(255) DEFAULT NULL,
  `amount_rent` int(11) DEFAULT NULL,
  `type_home` varchar(255) DEFAULT NULL,
  `s_home` varchar(255) DEFAULT NULL,
  `s_furniture` varchar(255) DEFAULT NULL,
  `note_home` longtext,
  `health` varchar(255) DEFAULT NULL,
  `s_health` longtext,
  `name_per` varchar(255) DEFAULT NULL,
  `job_per` varchar(255) DEFAULT NULL,
  `id_num_per` int(11) DEFAULT NULL,
  `relative_per` varchar(255) DEFAULT NULL,
  `no_agency` int(11) DEFAULT NULL,
  `date_agency` date DEFAULT NULL,
  `type_agency` varchar(255) DEFAULT NULL,
  `duration_agency` varchar(255) DEFAULT NULL,
  `source_agency` varchar(255) DEFAULT NULL,
  `teleph_per` int(11) DEFAULT NULL,
  `phone_per` int(11) DEFAULT NULL,
  `note_resarch` longtext,
  `note_social` longtext,
  `care_num` int(11) DEFAULT NULL,
  `type_care1` varchar(255) DEFAULT NULL,
  `model_care1` varchar(255) DEFAULT NULL,
  `farms` varchar(255) DEFAULT NULL,
  `animals` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `resarcher_id` int(11) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `employee` varchar(255) DEFAULT NULL,
  `wife` varchar(255) DEFAULT NULL,
  `type` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `perm_beneficiaries_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `social`;
CREATE TABLE `social` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Conditions` varchar(255) NOT NULL,
  `verification_elem` varchar(255) NOT NULL,
  `section` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `temp_beneficiaries_cat`;
CREATE TABLE `temp_beneficiaries_cat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `loc` varchar(255) NOT NULL,
  `telephone` int(11) NOT NULL,
  `phone` int(11) NOT NULL,
  `work` varchar(255) NOT NULL,
  `teleph_work` int(11) NOT NULL,
  `name_per` varchar(255) NOT NULL,
  `work_per` varchar(255) NOT NULL,
  `teleph_per` int(11) NOT NULL,
  `phone_per` int(11) NOT NULL,
  `telepho_per_work` int(11) NOT NULL,
  `m_income` int(11) NOT NULL,
  `y_income` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_resarch` int(11) DEFAULT NULL,
  `id_num_per` int(11) NOT NULL,
  `home` varchar(255) NOT NULL,
  `typehome` varchar(255) NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `temp_familly`;
CREATE TABLE `temp_familly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_b` int(11) NOT NULL,
  `child` varchar(255) CHARACTER SET utf8 NOT NULL,
  `child_num` int(11) NOT NULL,
  `reason` varchar(255) CHARACTER SET utf8 NOT NULL,
  `wife` varchar(255) CHARACTER SET utf8 NOT NULL,
  `car` int(11) NOT NULL,
  `farmer` varchar(22) CHARACTER SET utf8 NOT NULL,
  `animals` varchar(255) CHARACTER SET utf8 NOT NULL,
  `mosque` varchar(255) CHARACTER SET utf8 NOT NULL,
  `aimam` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `temp_individule`;
CREATE TABLE `temp_individule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_b` int(11) NOT NULL,
  `status` varchar(255) CHARACTER SET utf8 NOT NULL,
  `date_st` varchar(255) CHARACTER SET utf8 NOT NULL,
  `name_lessor` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `amount_rent` int(11) DEFAULT NULL,
  `type_mony` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `level` enum('1','2','3','4','5') CHARACTER SET latin1 DEFAULT '5',
  `password` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `charity_id` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2017-03-13 18:11:21
