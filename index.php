<?php

// include start file
require_once('start.php');

// Views Data
$data['active'] = 'home';

// Render Template
echo $twig->render('front/index.html', $data);
